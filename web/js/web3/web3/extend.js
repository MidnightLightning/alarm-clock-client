define(['module', './formatters', './../utils/utils', './method', './property'], function (module, formatters, utils, Method, Property) {
    'use strict';

    // TODO: refactor, so the input params are not altered.
    // it's necessary to make same 'extension' work with multiple providers
    var extend = function extend(web3) {
        /* jshint maxcomplexity:5 */
        var ex = function ex(extension) {

            var extendedObject;
            if (extension.property) {
                if (!web3[extension.property]) {
                    web3[extension.property] = {};
                }
                extendedObject = web3[extension.property];
            } else {
                extendedObject = web3;
            }

            if (extension.methods) {
                extension.methods.forEach(function (method) {
                    method.attachToObject(extendedObject);
                    method.setRequestManager(web3._requestManager);
                });
            }

            if (extension.properties) {
                extension.properties.forEach(function (property) {
                    property.attachToObject(extendedObject);
                    property.setRequestManager(web3._requestManager);
                });
            }
        };

        ex.formatters = formatters;
        ex.utils = utils;
        ex.Method = Method;
        ex.Property = Property;

        return ex;
    };

    module.exports = extend;
});