define(['module', './formatters', './type'], function (module, f, SolidityType) {
    'use strict';

    /**
     * SolidityTypeAddress is a prootype that represents address type
     * It matches:
     * address
     * address[]
     * address[4]
     * address[][]
     * address[3][]
     * address[][6][], ...
     */
    var SolidityTypeAddress = function SolidityTypeAddress() {
        this._inputFormatter = f.formatInputInt;
        this._outputFormatter = f.formatOutputAddress;
    };

    SolidityTypeAddress.prototype = new SolidityType({});
    SolidityTypeAddress.prototype.constructor = SolidityTypeAddress;

    SolidityTypeAddress.prototype.isType = function (name) {
        return !!name.match(/address(\[([0-9]*)\])?/);
    };

    module.exports = SolidityTypeAddress;
});