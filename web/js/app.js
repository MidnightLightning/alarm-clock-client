define(['react', 'react-dom', './AppView'], function (_react, _reactDom, _AppView) {
  'use strict';

  var _react2 = _interopRequireDefault(_react);

  var _reactDom2 = _interopRequireDefault(_reactDom);

  var _AppView2 = _interopRequireDefault(_AppView);

  function _interopRequireDefault(obj) {
    return obj && obj.__esModule ? obj : {
      default: obj
    };
  }

  // Draw application
  var appContainer = document.getElementById('app');
  _reactDom2.default.render(_react2.default.createElement(_AppView2.default, {}), appContainer);
});