import React from 'react';
import ReactDOM from 'react-dom';
import AppView from './AppView';

// Draw application
let appContainer = document.getElementById('app');
ReactDOM.render(
  React.createElement(AppView, {}),
  appContainer
);
